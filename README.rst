============================
My notes about various stuff
============================

BASH
====

* command completion:
   * ``brew install bash_completion``
   * Add this to ``.bash_profile`` or ``.bashrc``
   .. code-block:: bash

    if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion``
    fi

Kubernetes
==========

Installing ``minikube`` and ``kubectl``
----------------------------------------

MacOS
~~~~~~

* using ``vbox`` as driver for minikube
* read `kubectl install guide`_
* ``brew install minikube``
* ``brew install kubectl``
* setup command completion
   #. ``minikube completion bash > ~/.bash_completion-minikube``
   #. ``kubectl completion bash > ~/.bash_completuon-kubectl``
   #. update ``.bashrc`` or ``.bash_profile``
   
   .. code-block:: bash
   
    . ~/.bash_completion-minikube
    . ~/.bash_completion-kubectl

* using docker and localy built images
   * ``minikube start --insecure-registry localhost:5000`` - start vbox VM with minikube
   * ``. <(minikube docker-env)`` - changes env of the shell to docker living in minikube VM
   * to check if it works:
      * ``ssh minikube`` ``docker images`` - should give same as host machine
   * use ``imagePullPolicy: IfNotPresent`` for that crap on local
 
 
 




.. _`kubectl install guide`: https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl